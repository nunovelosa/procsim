.. PROCSIM documentation master file, created by
   sphinx-quickstart on Sun Oct  2 21:18:50 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PROCSIM's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

 
   procsimulator

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
