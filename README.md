# PROCSIM

The PROCSIM simulator is an open source python energy community simulator to develop and evaluate load balancing schemes designed to allow researchers of this field to easily create ECs datasets for multiple purposes as well as test and evaluate their load balancing strategies due to the existent lack of available datasets and also due to the hard task of evaluating and comparing such approaches. It uses a modular approach to allow researchers to reuse, replace or append new modules. The simulator includes integration with a consumption profiles generator, a tool to forecast weather data and a photovoltaic energy systems simulator. In addition, it contains a module which generates a synthetic dataset based on the other modules and a module which allows to implement the strategies. Finally, the simulator includes implementations of several energy-related metrics, which can be used to easily analyse the impact of the approaches in the communities.
<br/>
The PROCSIM was created with two core use cases in mind. First, it should enable the creation of new ECs datasets completely customized according to the desired objective with different sizes (i.e. 1 week, 2 months, 1 year, etc), allowing them to be used for multiple purposes. Second, it should provide a simple interface for testing, evaluating and comparing load balancing algorithms, as well as assessing its generalization. In order to do that, the PROCSIM simulator was developed in Python due to the availability of a vast set of libraries for simulating both household consumption as well as pv and wind generation. Furthermore, Python is becoming more popular and is increasingly being used for data science due to its simplicity and flexibility.
<br/>
<br/>
<strong>Setup:</strong><br/>
- pip install procsimulator<br/>
<br/>
<strong>Latest Version:</strong> 0.1.2<br/>
<strong>Available on Pypi:</strong> https://pypi.org/project/procsimulator/<br/>
<strong>Documentation:</strong> https://procsim.readthedocs.io/<br/>
<br/>
<strong>PROCSIM Paper:</strong> https://www.mdpi.com/1996-1073/16/4/1611<br/>
<strong>More Information:</strong> https://digituma.uma.pt/handle/10400.13/4925<br/>
<br/>
<strong>PROCSIM ECB (Graphical User Interface for PROCSIM):</strong> https://github.com/feelab-info/PROCSIM_ECB<br/>
<strong>PROCSIM ECs Simulation Scenarios:</strong> https://github.com/feelab-info/PROCSIM_ECs_Simulation_Scenarios
